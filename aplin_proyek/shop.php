<?php
  require_once("connection.php");
  session_start();

  $id_user=$_POST['id_user'];
  $gold;
  $gems;
  $goldCheckout=0;
  $gemCheckout=0;

  $sql = "select * from users where id = '$id_user'";
  $result = mysqli_query($conn, $sql);
  while($row=mysqli_fetch_assoc($result))
  {
  	$gold=$row['gold'];
  	$gems=$row['gem'];
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SHOP</title>
	<!-- STYLE -->
	<link rel="stylesheet" href="css/mapstyle.css">
</head>
<body>

	<div class="container">
		<div id="goldContainer">
			<h6 style="position: relative; top: 40px; left: 65px;" id='gold'><?php echo($gold)?></h6>
			<img src="Resources/Images/Gold.png" alt="" style='padding-left: 10px;'>
			<h6 style="position: relative; top: 45px; left: 65px;" id='gem'><?php echo($gems) ?></h6>
			<img src="Resources/Images/Gem.png" alt="" style='padding-left: 10px;'>
		</div>
		<br><br>
		<button class="btn btn-info" onclick="createBuilding('none')">BACK TO MAP</button>
	</div>

	<br>
	<div class="container">
		<div class="container-naga">
			<div class="jumbotron bg-secondary text-light">
				<h1>Naga</h1>
				<hr>
				<div class="card-deck">
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/Praetorian.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyDragon('Praetorian')">5000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/Frilled.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyDragon('Frilled')">5000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/Lancelot.png"  alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyDragon('Lancelot')">5000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/Katsumoto.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyDragon('Katsumoto')">5000 Gold</button>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="container-gems">
			<div class="jumbotron bg-secondary text-light">
				<h1>GEMS</h1>
				<hr>
				<div class="card-deck">
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGem(5)">5 Gem</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGem(10)">10 Gem</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/gemIcon.png"  alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGem(25)">25 Gem</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGem(100)">100 Gem</button>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="container-gold">
			<div class="jumbotron bg-secondary text-light">
				<h1>GOLD</h1>
				<hr>
				<div class="card-deck">
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGold(10000)">5 Gem to 10000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGold(25000)">10 Gem to 25000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/goldIcon.png"  alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGold(75000)">25 Gem to 75000 Gold</button>
					</div>
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
						<button class="btn btn-primary" onclick="buyGold(225000)">100 Gem to 225000 Gold</button>
					</div>
				</div>
			</div>
		</div>

		<div class="container-Checkout">
			<div class="jumbotron bg-secondary text-light">
			<h1>CHECKOUT</h1>
			<hr>
				<div class="container text-dark">
					<div class="jumbotron">
						<h2>Order Details</h2>
						<hr>
						<table class="table table-hover rounded table-light text-dark" id="tableCart">
							<thead class="thead-dark">
								<th>No.</th>
								<th>Item</th>
								<th>Harga (GEMS)</th>
							</thead>
							<tbody>
								
							</tbody>
						</table>
						<hr>
						<h3 id="total"  >Total: </h3>
						<h3 id="gemLeft">Gems After Purchase: </h3>
						<hr>
						<button class="btn btn-danger" onclick="clearCart()">Clear Cart</button>
						<button class="btn btn-success" data-toggle='modal' data-target='#confirmationModal'>Check Out</button>
					</div>
				</div>
			</div>
		</div>

	<!-- CHECKOUT CONFIMATION MODAL -->
	<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Checkout</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are You Sure?
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	        <button type="button" class="btn btn-success" onclick="checkout()" data-dismiss="modal">Confirm</button>
	      </div>
	    </div>
	  </div>
	</div>
</body>
<!-- GAMESCRIPT -->

<script>
	
 	/*CODINGAN BARU*/
 	var cart = new Array();
 	var tableCart = $("#tableCart").DataTable();
 	// VARIABLE GOLD DAN GEM YANG DIMILIKI USER SAAT INI
 	var gold = 0;
 	var gem = 0;

 	// HARGA TOTAL BELANJA
 	var total = 0;
 	// Gem dan gold Yang ada di cart
 	var buygem = 0;
 	var buygold = 0;


 	function buyGold(n)
 	{
 		// Beli GOLD
 		// loadGoldDanGems THEN CHECK
 		loadGoldAndGems();

 		if(n == 10000)
 		{
 			if(gem - total - 5 >= 0)
 			{
 				//ADD ITEM TO CART
 				cart.push({item: "10000 Gold",value: 10000, harga: 5});
 			}
 			else
 			{
 				alert("Gem Tidak Cukup");
 			}
 		}
 		else if(n == 25000)
 		{
 			if(gem - total - 10 >= 0)
 			{
 				//ADD ITEM TO CART
 				cart.push({item: "25000 Gold",value: 25000, harga: 10});
 			}
 			else
 			{
 				alert("Gem Tidak Cukup");
 			}
 		}
 		else if(n == 75000)
 		{
 			if(gem - total - 25 >= 0)
 			{
 				//ADD ITEM TO CART
 				cart.push({item: "75000 Gold",value: 75000, harga: 25});
 			}
 			else
 			{
 				alert("Gem Tidak Cukup");
 			}
 		}
 		else if(n == 225000)
 		{
 			if(gem - total - 100 >= 0)
 			{
 				//ADD ITEM TO CART
 				cart.push({item: "225000 Gold",value: 225000, harga: 100});
 			}
 			else
 			{
 				alert("Gem Tidak Cukup");
 			}
 		}
 		else
 		{
 			alert("Error Buy");
 		}
 		loadCart();
 	}

 	function buyGem(n)
 	{
 		// Beli GEMS
 		// loadGoldDanGems THEN CHECK
 		loadGoldAndGems();

 		// CHECK
 		cart.push({item: n + " Gems", value: 0, harga: 0});
 		buygem += n;

 		loadCart();
 	}

 	function buyDragon(type)
 	{
 		// Beli Naga
 		// loadGoldDanGems THEN CHECK
 		loadGoldAndGems();

 		// CHECK
 		if(gold - 5000 >= 0)
 		{
 			// beli naga
 			if(confirm("Buy This Dragon?"))
 			{
 				naga = type;
 				$.post("loadMap.php", function(data){$("#container").html(data);initMap(); addHoverEvent();addClickEvent();loadMap(); animateContainer();});
 			}
 		}
 		else
 		{
 			alert("Gold Tidak Cukup");
 		}
 	}

 	function loadCart()
 	{
 		
 		total = 0;
 		buygold = 0;
 		//update table
 		tableCart.clear();
 		for (var i = 0; i < cart.length; i++)
 		{
 			//update total
 			total += cart[i].harga;
 			buygold += cart[i].value;
 			tableCart.row.add([tableCart.rows().count() + 1,cart[i].item,cart[i].harga]).draw();
 		}

 		$("#total").html("Total : " + total + " Gems");
 		$("#gemLeft").html("Gems After Purchase: " + (gem-total+buygem));
 	}

 	function loadGoldAndGems()
 	{
 		// ID USER DAPAT DARI NEWMAP.php
 		$.post("load_gold_dan_gem.php",{id_user: id_user},function(data)
 		{
 			//SPLIT GOLD DAN GEMS
 			var result = data.split(";");
 			$('#gold').html(result[0]);
 			$('#gem').html(result[1]);

 			gold = parseInt(result[0]);
 			gem = parseInt(result[1]);
			
 		});
 	}

 	function checkout()
 	{
 		/*CHECK GOLD AND GEMS*/
 		loadGoldAndGems();

 		if(gem - total >= 0)
 		{
 			$.post("checkout.php",{gem: gem-total+buygem, gold: gold + buygold,id_user: id_user, buygem: buygem, buygold: buygold},function(){alert("Pembelian Sukses!"); clearCart();});
 		}
 		else
 		{
 			// Error jika uang tidak cukup (ANTI-BUG SYSTEM)
 			alert("Error Buy");
 		}

 		
 	}

 	function clearCart()
 	{
 		//clear Table
 		tableCart.clear();

 		// Reset variables
 		cart = new Array();
 		gold = 0;
	 	gem = 0;
	 	total = 0;
	 	buygem = 0;

	 	tableCart.draw();
	 	loadGoldAndGems();
 		loadCart();
 	}

 	loadGoldAndGems();
 	loadCart();

</script>


</html>
