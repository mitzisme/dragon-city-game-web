<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BUILD</title>
</head>
<body>
	<div class="container">
		<div id="goldContainer">
			<h6 style="position: relative; top: 40px; left: 65px;" id='gold'>10000</h6>
			<img src="Resources/Images/Gold.png" alt="" style='padding-left: 10px;'>
			<h6 style="position: relative; top: 45px; left: 65px;" id='gem'>10000</h6>
			<img src="Resources/Images/Gem.png" alt="" style='padding-left: 10px;'>
		</div>
		<br><br>
		<button class="btn btn-info" onclick="createBuilding('none')">Back To Map</button>
	</div>                  
	<br>
	<div class="container">
		<div class="jumbotron bg-secondary text-light">
			<h1>Gem -> Gold</h1>
			<hr>
			<div class="card-deck">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGold(1000,1)">1 Gems -> 10000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGold(2000,2)">2 Gems -> 2000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/goldIcon.png"  alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGold(3000,3)">3 Gems -> 3000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/goldIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGold(10000,7)">7 Gems -> 10000 Gold</a>
				</div>
			</div>
		</div>
		<div class="jumbotron bg-secondary text-light">
			<h1>Gold -> Gem</h1>
			<hr>
			<div class="card-deck">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGem(1,2000)">2000 Gold -> 1 Gems</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGem(2,5000)">5000 Gold -> 2 Gems</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/gemIcon.png"  alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGem(3,7500)">7500 Gold -> 3 Gems</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/gemIcon.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="buyGem(5,15000)">15000 Gold -> 5 Gems</a>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
</body>
</html>