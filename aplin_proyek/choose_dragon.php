<?php
	include "connection.php";
	$id_user=$_POST['id_user'];
	$naga;
	$hp_naga;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Choose Dragon</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron bg-dark">
			<h1 class="text-light">Your Dragons</h1>
			<hr>
			<div class="jumbotron">
				<table class="table table-hover rounded" id="tableUser">
				<thead class="thead-dark">
					<th>Nama Dragon</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php
						$sql= "SELECT * FROM naga where id_user='$id_user'";
						$res = $conn->query($sql);
						while($row = $res->fetch_assoc())
						{
							$naga=$row['nama_naga'];
							$level=$row['level'];
							$index_naga=$row['index_naga'];
							$hp_naga=$row['health_point'];
							echo "<tr class='table-light'>";
							echo "<td>$naga- level:$level</td>";
							echo '<td><button class="btn btn-success" onclick="choose('."'".$naga."'".','."'".$index_naga."','".$hp_naga."')".'"'.">Choose</button></td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
			<button class='btn btn-success' onclick="choose('praetorian')">Choose</button>
			</div>
		</div>
	</div>
	<div id="debug">
		
	</div>
</body>	

<script>
	var timer_wait_opponent;
	
	function wait_opponent_dragon()
	{
		$.post("waiting_opponent.php",{id_user: id_user}, 
		function(data){
			var lis_data=data.split(';');
			$("#debug").html(lis_data[0]);
			if(lis_data[0] == "done")
			{
				//STOP TIMER WAITING OPPONENT'S DRAGON
				clearInterval(timer_wait_opponent);
				alert("GET READY FOR THE NEXT FIGHTER");
				$.post("battle.php",{id_user: id_user,id_room:lis_data[1]},function(data){$('#container').html(data);animateContainer();});
			}
		});
	}
	
	
</script>
</html>
