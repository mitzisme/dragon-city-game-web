<?php
	require_once('connection.php');
	session_start();
	$userindex = $_POST['userindex'];
	$sql = "SELECT * FROM USERS WHERE ID = $userindex";
	$res = $conn->query($sql);
	$user = $res->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- SCIRPTS + BOOSTRAP -->
</head>
<body>
	<div class="container">
		<div class="jumbotron bg-dark">
			<h1 class="text-light">USERS</h1>
			<div class="jumbotron">
				<table class="table table-hover rounded" id="tableUser">
				<caption class="text-dark">Blue Row Is The Current Logged in User <br>Red Row Is Another Admin</caption>
				<thead class="thead-dark">
					<th>ID</th>
					<th>Email</th>
					<th>Password</th>
					<th>Name</th>
					<th>Gold</th>
					<th>Gem</th>
					<th>Win</th>
					<th>MMR</th>
				</thead>
				<tbody>
					<?php
						$sql = "SELECT * FROM USERS";
						$res = $conn->query($sql);
						while($row = $res->fetch_assoc())
						{
							if($row['id'] == $user['id'])
							{
								echo "<tr class='table-info' onclick='editUser($row[id])' data-toggle='tooltip' title='Click To Edit User'>";
							}
							else if($row['isadmin'] == 1)
							{
								echo "<tr class='table-danger' data-toggle='tooltip' title='Cannot Edit Another Admin'>";
							}
							else
							{
								echo "<tr class='table-light' onclick='editUser($row[id])' data-toggle='tooltip' title='Click To Edit User'>";
							}
							echo "<td>$row[id]</td>";
							echo "<td>$row[email]</td>";
							echo "<td>$row[password]</td>";
							echo "<td>$row[name]</td>";
							echo "<td>$row[gold]</td>";
							echo "<td>$row[gem]</td>";
							echo "<td>$row[win]</td>";
							echo "<td>$row[mmr]</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</body>
</html>