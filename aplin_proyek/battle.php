<?php
  require_once("connection.php");
  session_start();

  $id_user=$_POST['id_user'];
  $id_musuh;
  $id_room=$_POST['id_room'];
  $player=0;
  $naga_player='-';
  $naga_musuh='-';
  $hp_player;
  $hp_musuh;
  $turn=1;
  
  $sql = "select * from room where id = '$id_room'";
  $result = mysqli_query($conn, $sql);
  while($row=mysqli_fetch_assoc($result))
  {
	if($row['user1']==$id_user){
		$player=1;
		$naga_player=$row['naga1'];
		$hp_player=$row['hp_naga1'];
		$naga_musuh=$row['naga2'];
		$hp_musuh=$row['hp_naga2'];
		$id_musuh=$row['user2'];
	}
	else if($row['user2']==$id_user){
		$player=2;
		$naga_player=$row['naga2'];
		$naga_musuh=$row['naga1'];
		$id_musuh=$row['user1'];
	}
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SHOP</title>
	<!-- STYLE -->
	<link rel="stylesheet" href="css/mapstyle.css">
	<!-- SCIRPTS + BOOSTRAP -->
	<script src="Jquery/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
	<script src="Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container-battlefield">
		<div id="bg-arena" style="">
			<div id="naga_player">
			
			</div>
			
			<div id="naga_opponent">
			
			</div>
			
			<div id="turn-counter" style="top:200px;left:200px;color:white">
				PLAYER 1'S TURN 
			</div>
		</div>
		<br><br>
		<button class="btn btn-info" onclick="createBuilding('none')">BACK TO MAP</button>
	</div>
	
</body>
<!-- GAMESCRIPT -->

<script>
	$('#naga_player').css('background-image',"url('Resources/Images/<?php echo $naga_player?>.png')");
	$('#naga_opponent').css('background-image',"url('Resources/Images/<?php echo $naga_musuh?>-opponent.png')");
	
	var player=<?php echo $player?>;
	var naga_player='<?php echo $naga_player?>';
	var naga_musuh='<?php echo $naga_musuh?>';
	var turn=<?php echo $turn?>;
	var id_room=<?php echo$id_room?>;
	var id_player=<?php echo $id_user?>;
	var id_musuh=<?php echo $id_musuh?>;
	var timer_cek_serangan=setInterval(function(){cek_move()},1000);
	var action="-";
	var damage;
	
	function cek_move(){
		if(turn==player)
		{
			$('#turn-counter').html('YOUR TURN CHOOSE YOUR ACTION!');
			if(action!="-"){
				alert("you used :"+action);
				$.post
				("update_turn.php", {id_room:id_room,turn_player:player,action:action},
					function(data)
					{
						alert("END TURN");
						$.post
						("update_hp_dragon.php", {player:player,damage:damage},
							function(data)
							{
								alert(data);
								if(player==1)
								{
									if(data<=0)
									{
										alert("player1 menang");
										$.post
										("drop_room.php", {id_room:id_room},
											function()
											{
												$.post
												("update_MMR.php", {id_musuh:id_musuh,id_player:id_user,index_menang:1},
													function()
													{
														
													}
												);
											}
										);
										clearInterval(timer_cek_serangan);
										createBuilding('none');
									}
								}
								else if(player==2)
								{
									if(data<=0)
									{
										alert("player2 menang");
										$.post
										("drop_room.php", {id_room:id_room},
											function()
											{
												$.post
												("update_MMR.php", {id_musuh:id_musuh,id_player:id_user,index_menang:2},
													function()
													{
														
													}
												);
											}
										);
										clearInterval(timer_cek_serangan);
										createBuilding('none');
									}
								}
							}
						);
						$.post
						("reset_turn.php", {id_room:id_room},
							function()
							{
								action='-';
								damage=0;
							}
						);
						$.post
						("get_turn.php", {id_room:id_room},
							function(data)
							{
								turn=data;
							}
						);
					}
				);
			}
		}
		else
		{
			$('#turn-counter').html("ENEMY'S TURN YOU MUST WAIT FOR THEIR ACTION");
			$.post
			("cek_move_musuh.php", {id_room:id_room,turn_player:player},
				function(data)
				{
					if(data!='-')
					{
						alert("stts ENEMY JUST USED "+data+ " WE MUST NOT TAKE THIS THING");
						$.post
						("cek_mati.php", {id_room:id_room,player:player},
							function(data)
							{
								if(data<=0)
								{
									alert("stts your dragon has LOST try again next time byeeee");
									clearInterval(timer_cek_serangan);
									$.post
										("drop_room.php", {id_room:id_room},
											function()
											{
												
											}
										);
									createBuilding('none');
								}
								else{
									$.post
									("get_turn.php", {id_room:id_room},
										function(data)
										{
											turn=data;
										}
									);
								}
							}
						);
					}
				}
			);
		}
	}
	
	document.onkeydown = function(e){
		if(naga_player=="Praetorian1")
		{
			if(e.key == "ArrowRight")
			{
				action="Knight's smash";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				alert("naganya belum punya skill 2");
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
				damage=30;
			}
		}
		else if(naga_player=="Praetorian2")
		{
			if(e.key == "ArrowRight")
			{
				action="warrior's smash";
				damage=10
			}
			else if(e.key == "ArrowLeft")
			{
				action="Inner fire blast";
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
				damage=30;
			}
		}
		else if(naga_player=="Praetorian3")
		{
			if(e.key == "ArrowRight")
			{
				action="warrior's smash";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				action="Inner fire blast";
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				action="7 star slash";
				damage=30;
			}
		}
		else if(naga_player=="Frilled1")
		{
			if(e.key == "ArrowRight")
			{
				action="psychic pressure";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				alert("naganya belum punya skill 2");
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
				damage=30;
			}
		}
		else if(naga_player=="Frilled2")
		{
			if(e.key == "ArrowRight")
			{
				action="psychic pressure";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				action="Void crush";
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
				damage=30;
			}
		}
		else if(naga_player=="Frilled3")
		{
			if(e.key == "ArrowRight")
			{
				action="psychic pressure";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				action="Void crush";
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				action="Midnight arrow";
				damage=30;
			}
		}
		else if(naga_player=="Lancelot1")
		{
			if(e.key == "ArrowRight")
			{
				action="Lance a lot";
				damage=10;
			}
			else if(e.key == "ArrowLeft")
			{
				alert("naganya belum punya skill 2");
				damage=20;
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
				damage=30;
			}
		}
		else if(naga_player=="Lancelot2")
		{
			if(e.key == "ArrowRight")
			{
				action="Lance a lot";
			}
			else if(e.key == "ArrowLeft")
			{
				action="Final Elysion";
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
			}
			damage=50;
		}
		else if(naga_player=="Lancelot3")
		{
			if(e.key == "ArrowRight")
			{
				action="Lance a lot";
			}
			else if(e.key == "ArrowLeft")
			{
				action="Final Elysion";
			}
			else if(e.key == "ArrowUp"){
				action="Zeig Saber";
			}
			damage=50;
		}
		else if(naga_player=="Katsumoto1")
		{
			if(e.key == "ArrowRight")
			{
				action="Bushido burst";
			}
			else if(e.key == "ArrowLeft")
			{
				alert("naganya belum punya skill 2");
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
			}
			damage=50;
		}
		else if(naga_player=="Katsumoto2")
		{
			if(e.key == "ArrowRight")
			{
				action="Bushido burst";
			}
			else if(e.key == "ArrowLeft")
			{
				action="Nova striker";
			}
			else if(e.key == "ArrowUp"){
				alert("naganya belum punya skill 3");
			}
			damage=50;
		}
		else if(naga_player=="Katsumoto3")
		{
			if(e.key == "ArrowRight")
			{
				action="Bushido burst";
			}
			else if(e.key == "ArrowLeft")
			{
				action="Nova striker";
			}
			else if(e.key == "ArrowUp"){
				action="Magnum brake";
			}
			damage=50;
		}
	}
</script>


</html>
