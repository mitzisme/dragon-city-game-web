<!DOCTYPE html>
<?php
  require_once("connection.php");
  session_start();

  $id_user=$_POST['id_user'];
  $gold;
  $gems;
  $goldCheckout=0;
  $gemCheckout=0;

  $sql = "select * from users where id = '$id_user'";
  $result = mysqli_query($conn, $sql);
  while($row=mysqli_fetch_assoc($result))
  {
  	$gold=$row['gold'];
  	$gems=$row['gem'];
  }
?>


<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BUILD</title>
</head>
<body>
	<div class="container">
		<div id="goldContainer">
			<h6 style="position: relative; top: 40px; left: 65px;" id='gold'><?php echo($gold)?></h6>
			<img src="Resources/Images/Gold.png" alt="" style='padding-left: 10px;'>
			<h6 style="position: relative; top: 45px; left: 65px;" id='gem'><?php echo($gems)?></h6>
			<img src="Resources/Images/Gem.png" alt="" style='padding-left: 10px;'>
		</div>
		<br><br>
		<button class="btn btn-info" onclick="createBuilding('none')">Back To Map</button>
	</div>                  
	<br>
	<div class="container">
		<div class="jumbotron bg-secondary text-light">
			<h1>Building</h1>
			<hr>
			<div class="card-deck">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/terra_habitat.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="createBuilding('terra_habitat')">5000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/breeding.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="createBuilding('breeding')">5000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/farm.png"  alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="createBuilding('farm')">5000 Gold</a>
				</div>
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="Resources/Images/hatchery.png" alt="Card image cap" style="height: 10rem;">
					<a href="#" class="btn btn-primary" onclick="createBuilding('hatchery')">5000 Gold</a>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
</body>
</html>