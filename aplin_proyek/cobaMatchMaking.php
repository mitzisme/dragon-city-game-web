<?php
	require_once("connection.php");
	session_start();

	$id_user=$_POST['id_user'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body class="bg-dark">
	<div class="container">
		<div class="jumbotron">
			<button class="btn btn-primary" onclick="findMatch()">
			find match
			</button>
			<button class="btn btn-primary" onclick="createBuilding('none')">
			Back to map
			</button>
		</div>
	</div>
	<div id="debug">
		
	</div>
</body>

<script>
	var id_user = <?php echo $id_user?>;
	
	// UNTUK FIND MATCH
	var findingMatch;
	var findTime = 0;
	var offsetMMR = 25;

	function findMatch()
	{
		$.post("find_match.php",{id_user: id_user},function(data)
		{
			$("#debug").html(data);
			alert("finding match...");
			findingMatch = setInterval(
				function()
				{
					refreshMatchMaking();
					console.log("Refreshed MATCHMAKING");
				},1000);
		});
	}

	function refreshMatchMaking()
	{
		// Finding match
		findTime++;

		if(findTime == 10)
		{
			offsetMMR += 25;
			findTime = 0;
		}

		$.post("get_opponent.php",{id_user: id_user, offsetMMR: offsetMMR}, 
			function(data){
				$("#debug").html(data);
				if(data != "none")
				{
					//STOP TIMER MATCHMAKING
					clearInterval(findingMatch);
					alert("FOUND OPPONENT AT ROOM" + data);
					choose_dragon();
				}
			});
	}



</script>

</html>