<!DOCTYPE html>
<?php
  require_once("connection.php");
  session_start();

  $id_user=$_POST['id_user'];
  $food = 0;

  $sql = "select * from users where id = '$id_user'";
  $result = mysqli_query($conn, $sql);
  while($row=mysqli_fetch_assoc($result))
  {
  	$food=$row['food'];
  }

?>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>FARM</title>
</head>
<body>
	<br>
	<div class="container">
		<button class="btn btn-info" onclick="createBuilding('none')">BACK TO MAP</button>
		<br>
		<br>
		<div id="foodContainer">
			<h6 style="position: relative; top: 40px; left: 65px;" id='food'><?php echo($food)?></h6>
			<img src="Resources/Images/Gold.png" alt="" style='padding-left: 10px;'>
		</div>
		<br><br>
		<div class="jumbotron">
			<h1>Buy Food</h1>
			<hr>
			<div class="card-deck">
				<div class="card" style="width: 18rem">
					<img class="card-img-top" src="css/Resources/Images/food.png">
					<button id='btnHabitat' class="btn btn-primary" onclick="createFood('5')">5-100gold</button>
				</div>
				
				<div class="card" style="width: 18rem">
					<img class="card-img-top" src="css/Resources/Images/food.png">
					<button id='btnFood' class="btn btn-primary" onclick="createFood('10')">10-200gold</button>
				</div>
				
				<div class="card" style="width: 18rem">
					<img class="card-img-top" src="css/Resources/Images/food.png">
					<button id='btnHatchery' class="btn btn-primary" onclick="createFood('15')">15-300gold</button>
				</div>
				
				<div class="card" style="width: 18rem">
					<img class="card-img-top" src="css/Resources/Images/food.png">
					<button id='btnBreedingMountain' class="btn btn-primary" onclick="createFood('20')">20-400gold</button>
				</div>
			</div>
		</div>	
	</div>
</body>
</html>