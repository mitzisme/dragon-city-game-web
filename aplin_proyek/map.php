<?php
	session_start();
	
	$bangun_habitat='false';
	$bangun_food='false';
	$bangun_hatchery='false';
	$bangun_breeding_mountain='false';
	
	if(!isset($_SESSION['bangunan'])){
		$_SESSION['bangunan']=[];
	}
	else {
		if($_SESSION['bangunan']['habitat']==true){
			$bangun_habitat='true';
		}
		if($_SESSION['bangunan']['food']==true){
			$bangun_food='true';
		}
		
		if($_SESSION['bangunan']['hatchery']==true){
			$bangun_hatchery='true';
		}
		
		if($_SESSION['bangunan']['breeding_mountain']==true){
			$bangun_breeding_mountain='true';
		}
	}
?>

<html>
	<head>
		<title>map</title>
		
		<base href="/aplin_proyek/">
		
		<script src="jquery-3.3.1.min.js"></script>
		
		<style>
			#background_map{
				width:700px;
				height:700px;
				background-image:URL("Resources/Images/MAP.png");
				background-size:100% 100%;
				background-repeat: no-repeat;
				
				
			}
			.tanah{
				width:100px;
				height:100px;
				float:left;
				background-size:100% 100%;
				background-repeat: no-repeat;
				position:absolute;
				
			}
			#orang{
				width:50px;
				height:50px;
				background-size:100% 100%;
				background-repeat: no-repeat;
				position:absolute;
			}
			#uang{
				width:100px;
				height:50px;
				background-image:URL("Resources/Images/GOLD.png");
				background-size:100% 100%;
				background-repeat: no-repeat;
				float:left;
			}
		</style>
	</head>
	
	<body>
		<div id="background_map" >
			<div id="uang">
			
			</div>
		</div>
		
		<div id="orang" ></div>
	</body>
</html>


<script>
	
	var map_baris1 =[0,0,0,0,0];
	var map_baris2 =[0,0,0,0,0];
	var map_baris3 =[0,0,0,0,0];
	var map_baris4 =[0,0,0,0,0];
	var map_baris5 =[0,0,0,0,0];
	
	var map=[map_baris1,map_baris2,map_baris3,map_baris4,map_baris5];
	
	//LOAD DATABASE ISI BANGUNAN
	$.post 
	("cek_bangunan.php", {}, 
		function(hasil)
		{
			var lis_bangunan=hasil.split(';');
			for(var i=0;i<lis_bangunan.length-1;i++){
				var bangunan=lis_bangunan[i].split(',');
				var x=bangunan[0];
				var y=bangunan[1];
				var jenis=bangunan[2];
				map[y][x]=jenis;
			}
		}
	);
	
	//jangan hilangkan alert ini
	alert("taruk dimana");
	
	//CEK BANGUN APA
	var bangun_food=<?php echo($bangun_food)?>;
	var bangun_hatchery=<?php echo($bangun_hatchery)?>;
	var bangun_breeding_mountain=<?php echo($bangun_breeding_mountain)?>;
	var bangun_habitat=<?php echo($bangun_habitat)?>;
	
	var nama_bangunan;
	
	$(document).ready(function(){
		//ISI MAP
		for(var i=0;i<5;i++){
			for(var j=0;j<5;j++){
				if(map[i][j]==1){
					var anak=$('<div class="tanah" index_i='+i+' index_j='+j+' style="width:100px;height:100px;left:'+(j*100)+'px;top:'+(i*100)+'px;background-image:url(Resources/Images/farm.png)"></div>')
					//$("body").prepend(anak);
					$("#background_map").append(anak);
				}
				if(map[i][j]==2){
					var anak=$('<div class="tanah" index_i='+i+' index_j='+j+' style="width:100px;height:100px;left:'+(j*100)+'px;top:'+(i*100)+'px;background-image:url(Resources/Images/breeding.png)"></div>')
					//$("body").prepend(anak);
					$("#background_map").append(anak);
				}
				if(map[i][j]==3){
					var anak=$('<div class="tanah" index_i='+i+' index_j='+j+' style="width:100px;height:100px;left:'+(j*100)+'px;top:'+(i*100)+'px;background-image:url(Resources/Images/terra_habitat.png)"></div>')
					//$("body").prepend(anak);
					$("#background_map").append(anak);
				}
				if(map[i][j]==4){
					var anak=$('<div class="tanah" index_i='+i+' index_j='+j+' style="width:100px;height:100px;left:'+(j*100)+'px;top:'+(i*100)+'px;background-image:url(Resources/Images/Hatchery.png)"></div>')
					//$("body").prepend(anak);
					$("#background_map").append(anak);
				}
				else{
					var anak=$('<div class="tanah" index_i='+i+' index_j='+j+' style="width:100px;height:100px;left:'+(j*100)+'px;top:'+(i*100)+'px"></div>')
					//$("body").prepend(anak);
					$("#background_map").append(anak);
				}
			}
		}
		
		//HOVER TANAH
		$(".tanah").hover(
			function(){
				$(this).css("outline-style","solid");
				$(this).css("outline-color","white");
			},
			function(){
				$(this).css("outline-style","");
				$(this).css("outline-color","");
			}
		);
		
		$(".tanah").click(
			function(){
				if(map[$(this).attr("index_i")][$(this).attr("index_j")]==0){
					if(bangun_food==true){
						$(this).css('background-image','url("Resources/Images/farm.png")');
						<?php
							$_SESSION['bangunan']['food']=false;
							$bangun_food='false';
							
						?>
						bangun_food=false;
						
						//ISI DATABASE FARM
						nama_bangunan=1;
						$.post
						("insert_bangunan.php", {nama_bangunan: nama_bangunan,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j")}, 
							function()
							{
								alert("masukkk");
							}
						);
					}
					else if(bangun_breeding_mountain==true){
						$(this).css('background-image','url("Resources/Images/breeding.png")');
						<?php
							$_SESSION['bangunan']['breeding_mountain']=false;
							$bangun_breeding_mountain='false';
							
						?>
						bangun_breeding_mountain=false;
						
						//ISI DATABASE BREEDING MOUNTAIN
						nama_bangunan=2;
						$.post
						("insert_bangunan.php", {nama_bangunan: nama_bangunan,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j")}, 
							function()
							{
								alert("masukkk");
							}
						);
					}
					else if(bangun_habitat==true){
						$(this).css('background-image','url("Resources/Images/terra_habitat.png")');
						<?php
							$_SESSION['bangunan']['habitat']=false;
							$bangun_habitat='false';
							
						?>
						bangun_habitat=false;
						
						//ISI DATABASE HABITAT
						nama_bangunan=3;
						$.post
						("insert_bangunan.php", {nama_bangunan: nama_bangunan,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j")}, 
							function()
							{
								alert("masukkk");
							}
						);
					}
					else if(bangun_hatchery==true){
						$(this).css('background-image','url("Resources/Images/Hatchery.png")');
						<?php
							$_SESSION['bangunan']['hatchery']=false;
							$bangun_hatchery='false';
							
						?>
						bangun_hatchery=false;
						
						//ISI DATABASE Hatchery
						nama_bangunan=4;
						$.post
						("insert_bangunan.php", {nama_bangunan: nama_bangunan,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j")}, 
							function()
							{
								alert("masukkk");
							}
						);
					}
					else{
						location.replace("build.php");
					}
				}
				else{
					if(map[$(this).attr("index_i")][$(this).attr("index_j")]==1){
						
					}
				}
			}
		);
	});
	
	$(document).mousemove(function(event){
		//CURSOR HOVER
		if(bangun_food==true){
			$('#orang').css('background-image','url("Resources/Images/farm.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else if(bangun_breeding_mountain==true){
			$('#orang').css('background-image','url("Resources/Images/breeding.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else if(bangun_hatchery==true){
			$('#orang').css('background-image','url("Resources/Images/Hatchery.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else if(bangun_habitat==true){
			$('#orang').css('background-image','url("Resources/Images/terra_habitat.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else{
			$('#orang').css('background-image','url("")');
		}
	});
</script>