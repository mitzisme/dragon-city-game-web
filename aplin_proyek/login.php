<?php
	require_once("connection.php");
	session_start();
	$sukseslogin = -2;

	$_SESSION['userindex']=[];
	if(isset($_POST['inputEmail']))
	{
		//checkEmail
		$email = $_POST['inputEmail'];
		$password = $_POST['inputPassword'];

		$sql = "SELECT * FROM users WHERE email LIKE '$email'";

		$result = $conn->query($sql);
		if(mysqli_num_rows($result) > 0)
		{
			//login
			$user = $result->fetch_assoc();

			//var_dump($user);
			if($user['password'] == $password)
			{
				//login
				$_SESSION['userindex'] = $user['id'];
				if($user['isadmin'])
				{
					header("location: admin.php");
				}
				else
				{
					header("location: newmap.php");	
				}
				$sukseslogin = 1;
			}
			else
			{
				$sukseslogin = 0;
			}
		}
		else
		{
			$sukseslogin = -1;
		}
	}
?>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<!-- SCIRPTS + BOOSTRAP -->
	<script src="Jquery/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
	<script src="Bootstrap/js/bootstrap.min.js"></script>
</head>
<body background="Resources/Images/bg1.jpg" style="background-size: 100%">
	
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <a class="navbar-brand" href="#">Dragon City</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Login <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="register.php">Register</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container col-md-3">
	<?php
	if(isset($sukseslogin))
	{
		if($sukseslogin == 0)
		{
			echo "<div class='alert alert-danger text-center' id='alert' role='alert'>
				  Password Salah!
				</div>";
		}
		else if($sukseslogin == -1)
		{

			echo "<div class='alert alert-warning text-center' id='alert' role='alert'>
				  Username Tidak Ditemukan!
				</div>";
		}
	}
	?>
    </div>

    <main role="main" class="container">
      <div style="float:none;margin:0 auto" class="jumbotron col-md-6">
         <form class="form-signin" method="POST" action="">
	      <div class="text-center mb-4">
	      <div class="form-label-group">
	       	<h6 class="text-left">Email address: </h6>
	        <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
	      </div>
			<br>
	      <div class="form-label-group">
	      	<h6 class="text-left">Password: </h6>
	        <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
	      </div>
			<br>
	      <div class="checkbox mb-3 ">
	        <label>
	          <input type="checkbox" value="remember-me"> Remember me
	        </label>
	      </div>
	      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
	      <p class="mt-5 mb-3 text-muted text-center">&copy; 2018</p>
	    </form>
      </div>
    </main>
    <script>
    	var sukseslogin = <?php echo $sukseslogin;?>;

    	if(sukseslogin == -1 || sukseslogin == 0)
    	{
    		$("#alert").fadeTo(2000, 500).slideUp(500, function(){
		    $("#alert").slideUp(500);
		});
    	}

    </script>
</body>
</html>