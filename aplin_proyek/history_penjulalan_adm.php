<?php
	require_once('connection.php');
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>HISTORY PENJUALAN</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron bg-dark">
			<h1 class="text-light">History Penjualan</h1>
			<hr>
			<div class="jumbotron">
				<table class="table table-hover rounded" id="tableUser">
				<thead class="thead-dark">
					<th>ID</th>
					<th>Email</th>
					<th>Kode</th>
					<th>Nilai</th>
				</thead>
				<tbody>
					<?php
						$sql = "SELECT s.index_shop, s.kode, s.nilai, s.banyak, u.email FROM SHOP s INNER JOIN USERS u ON u.id = s.id_user ORDER BY 1 ASC";
						$res = $conn->query($sql);
						while($row = $res->fetch_assoc())
						{
							echo "<tr class='table-light'>";
							echo "<td>$row[index_shop]</td>";
							echo "<td>$row[email]</td>";
							echo "<td>$row[kode]</td>";
							echo "<td>$row[nilai]</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</body>
</html>