<?php
session_start();

$build;
if(isset($_SESSION['build']))
{
	$build = $_SESSION['build'];
}
else
{
	$build = "none";
}

if(isset($_POST['btnLogout']))
{
	session_destroy();
	header("location: login.php");
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MAP</title>
	<!-- SCIRPTS + BOOSTRAP -->
	<script src="Jquery/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
	<script src="Bootstrap/js/bootstrap.min.js"></script>
	<!-- STYLE -->
	<link rel="stylesheet" href="css/mapstyle.css">
	<link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
	<script type="text/javascript" src="DataTables/datatables.min.js"></script>
</head>
<body class="bg-dark" id='container'>
	<div id="bg-map">

	</div>
	<div id="orang">

	</div>

</body>

<!-- GAMESCRIPT -->
<script>
	var map_baris1 =[0,0,0,0,0];
	var map_baris2 =[0,0,0,0,0];
	var map_baris3 =[0,0,0,0,0];
	var map_baris4 =[0,0,0,0,0];
	var map_baris5 =[0,0,0,0,0];
	var map = [map_baris1,map_baris2,map_baris3,map_baris4,map_baris5];


	var map_naga_baris1 =[0,0,0,0,0];
	var map_naga_baris2 =[0,0,0,0,0];
	var map_naga_baris3 =[0,0,0,0,0];
	var map_naga_baris4 =[0,0,0,0,0];
	var map_naga_baris5 =[0,0,0,0,0];
	var map_naga = [map_naga_baris1,map_naga_baris2,map_naga_baris3,map_naga_baris4,map_naga_baris5];

	// Variable Klik Hatch (Button Sukes)
	var index_i_sukses = -1;
	var index_j_sukses = -1;

	// String Build Apa ?
	var build = "<?php echo $build;?>";
	var id_user=<?php echo($_SESSION['userindex']) ?>;
	var naga="none";
	var jum_food=0;
	var timer_breeding;
	var battle=false;
	
	cek_bangunan();

	cek_habitat();

	//INISIALISASI MAP
	function initMap()
	{
		var offsetAtas = 50;
		var offsetKiri = 50;

		//ISI MAP
		for (var i = 0; i < 5; i++)
		{
			for (var j = 0; j < 5; j++)
			{
				var left = j * 100 + offsetKiri;
				var top = i *100 + offsetAtas;
				$("#bg-map").append("<div class='tanah' index_i='" + i+"' index_j = '"+ j +"' style='left: "+left+"px; top: "+ top+"px;' id='"+ i + "-" + j +"'></div>");
			}
		}
	}

	function loadMap()
	{
		//LOAD MAP DARI ARRAY BUKAN DARI DATABASE!
		for (var i = 0; i < 5; i++)
		{
			for (var j = 0; j < 5; j++)
			{
				if(map[i][j] == 1)
				{
					// farm
					$("#" + i + "-" + j).addClass("farm");
					$("#" + i + "-" + j).css('background-image',"url('Resources/Images/farm.png')");
				}
				else if(map[i][j] == 2)
				{
					// Breeding Mountain
					$("#" + i + "-" + j).addClass("breeding");
					$("#" + i + "-" + j).css('background-image',"url('Resources/Images/breeding.png')");
				}
				else if(map[i][j] == 3)
				{
					// Habitat
					$("#" + i + "-" + j).addClass("habitat");
					$("#" + i + "-" + j).css('background-image',"url('Resources/Images/terra_habitat.png')");
					$("#" + i + "-" + j).attr('uang','0');
					if(map_naga[i][j] == 'Praetorian1')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Praetorian1.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Praetorian2')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Praetorian2.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Praetorian3')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Praetorian3	.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Frilled1')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Frilled1.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Frilled2')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Frilled2.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Frilled3')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Frilled3.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Lancelot1')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Lancelot1.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Lancelot2')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Lancelot2.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Lancelot3')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Lancelot3.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Katsumoto1')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Katsumoto1.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Katsumoto2')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Katsumoto2.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
					else if(map_naga[i][j] == 'Katsumoto3')
					{
						$("#" + i + "-" + j).html("<img src='Resources/Images/Katsumoto3.png' style='width:50px;height:50px;left:20px;position:absolute;'>");
					}
				}
				else if(map[i][j] == 4)
				{
					// Hatchery
					$("#" + i + "-" + j).addClass("hatchery");
					$("#" + i + "-" + j).css('background-image',"url('Resources/Images/hatchery.png')");
				}
			}
		}
	}

	/*DOCUMENT READY*/
	$.post("loadMap.php", function(data){$("#container").html(data);initMap(); addHoverEvent();addClickEvent();loadMap(); $("#container").css('display','none'); $("#container").fadeIn(500);});

	function addHoverEvent()
	{
		//TANAH HOVER
		$(".tanah").hover
		(
			function()
			{
				// JIKA DI HOVER MAKA OUTLINE PUTIH
				console.log("hover");
				$(this).css("outline-style","solid");
				$(this).css("outline-color","white");
			},
			function()
			{
				// CALLBACK FUNCTION (JIKA TIDAK DI HOVER)
				$(this).css("outline-style","");
				$(this).css("outline-color","");
			}
			);
	}

	//ADDING CLICK FUNCTION TO MAP
	function addClickEvent()
	{
		// TANAH CLICK
		$(".tanah").click
		(
			function()
			{
				//CLICKING TANAH WITHOUT BUILDING
				if(map[$(this).attr('index_i')][$(this).attr('index_j')]==0)
				{
					if(build == "none")
					{
						console.log("none");
						//Klik To Shop
						$.post("buildShop.php",{id_user: id_user},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
					}
					else
					{
						// Check String Build Apa
						if(build == "farm")
						{
							//build farm
							$.post
							("insert_bangunan.php", {index_bangunan: 1,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j"), id_user: id_user},
								function(data)
								{
									if(data=="gagal"){
										alert("uang tidak cukup");
									}
									cek_bangunan();
								}
							);
						}
						else if(build == "breeding")
						{
							//build breeding mountain
							$.post
							("insert_bangunan.php", {index_bangunan: 2,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j"), id_user: id_user},
								function(data)
								{
									if(data=="gagal"){
										alert("uang tidak cukup");
									}
									cek_bangunan();
								}
							);
						}
						else if(build == "terra_habitat")
						{
							// build habitat
							$.post
							("insert_bangunan.php", {index_bangunan: 3,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j"), id_user: id_user},
								function(data)
								{
									if(data=="gagal"){
										alert("uang tidak cukup");
									}
									cek_bangunan();
								}
							);
						}
						else if(build == "hatchery")
						{
							// build hatchery
							$.post
							("insert_bangunan.php", {index_bangunan: 4,index_i: $(this).attr("index_i"),index_j:$(this).attr("index_j"), id_user: id_user},
								function(data)
								{
									if(data=="gagal"){
										alert("uang tidak cukup");
									}
									cek_bangunan();
								}
							);
						}
						
						loadMap();
						
						build = "none";
					}
				}
				else
				{
					if(naga != "none")
					{
						if(map[$(this).attr('index_i')][$(this).attr('index_j')] == 2)
						{
							// CLICK BREEDING MOUNTAIN
							if(naga == "Praetorian" || naga == "Frilled" || naga == "Lancelot"|| naga== "Katsumoto")
							{
								// EGG IN HAND
								$.post
								("breeding_naga.php", {nama_naga: naga,koor_y: $(this).attr("index_i"),koor_x:$(this).attr("index_j"), id_user: id_user},
									function()
									{
										naga='none';
										// KURANGI GOLD PLAYER [KARENA PLAYER BELI TELUR DI SHOP] (?)
									}
								);
							}
							else
							{
								alert("Dragon Must Be Placed On Habitat!");
							}
						}
						else if(map[$(this).attr('index_i')][$(this).attr('index_j')] == 3)
						{
							// CLICK HABITAT 
							if(naga == "Praetorian1" || naga == "Frilled1"|| naga== "Lancelot1"|| naga== "Katsumoto1")
							{
								// VARIABEL INDEX I DAN J
								var index_i = $(this).attr("index_i");
								var index_j = $(this).attr("index_j");

								console.log(index_i + " - " + index_j);

								// NAGA IN HAND
								map_naga[$(this).attr('index_i')][$(this).attr('index_j')] = naga;
								$.post
								("insert_naga.php", {nama_naga: naga,koor_y: index_i,koor_x:index_j, id_user: id_user},
									function()
									{
										// REMOVE NAGA FROM HATCHERY
										$.post("remove_naga.php",
											{
												nama_naga: naga,
												koor_y: index_i_sukses,
												koor_x: index_j_sukses, 
												id_user: id_user
											},
											function(data)
											{
												// RESET INDEX SUKSES
												index_i_sukses = -1;
												index_j_sukses = -1;
											}); 
									}
									);
								naga = 'none';
							}
							else
							{
								alert("Egg Must Be Placed On breeding_mountain!");
							}
						}
						else if(naga == "Praetorian1" || naga == "Frilled1"|| naga== "Lancelot1"|| naga== "Katsumoto1")
						{
							alert("Dragon Must Be Placed On Habitat!");
						}
						else if(naga == "Praetorian1" || naga == "Frilled1"|| naga== "Lancelot1"|| naga== "Katsumoto1")
						{
							alert("Egg Must Be Placed On breeding_mountain!");
						}
					}
					else
					{
						//CLICKING FARM
						if(map[$(this).attr('index_i')][$(this).attr('index_j')]==1)
						{
							$.post("click_farm.php",{id_user: id_user},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
						}
						//CLICKING BREEDING MOUNTAIN
						else if(map[$(this).attr('index_i')][$(this).attr('index_j')]==2)
						{
							$.post("breed_naga.php",{koor_y:$(this).attr("index_i"),koor_x:$(this).attr("index_j"),id_user: id_user},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
						}
						//CLICKING HABITAT
						else if(map[$(this).attr('index_i')][$(this).attr('index_j')]==3)
						{
							if(map_naga[$(this).attr('index_i')][$(this).attr('index_j')]!='')
							{
								$.post("claim_gold.php",{id_user: id_user,koor_x:$(this).attr("index_j"),koor_y: $(this).attr("index_i")},function(data){alert("Mendapatkan " + data + " gold");});
								$.post("give_food.php",{id_user: id_user,koor_x:$(this).attr("index_j"),koor_y: $(this).attr("index_i")},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
							}
						}
						//CLICKING BATTLE
						else if(map[$(this).attr('index_i')][$(this).attr('index_j')]==4)
						{
							$.post("cobaMatchMaking.php",{id_user: id_user},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
						}
					}
					
				}
			}
			);
}
	//PLACING BULDING ON THE MAP
	function createBuilding(type)
	{
		clearInterval(timer_breeding);
		cek_habitat();
		$.post("loadMap.php",function(data){build = type; $("#container").html(data); initMap(); addHoverEvent();addClickEvent(); animateContainer();});
	}
	//GO TO SHOP
	function gotoshop()
	{
		$.post("shop.php",{id_user: id_user},function(data){$('#container').html(data);animateContainer();});
	}

	//GO TO DRAGON SELECTION
	function choose_dragon()
	{
		$.post("choose_dragon.php",{id_user: id_user},function(data){$('#container').html(data);animateContainer();});
	}

	//GET DRAGON FROM DRAGON SELECTION
	function choose(nama_naga,index_naga,hp_naga)
	{
		$.post
		("update_chosen_dragon.php", {id_user: id_user,index_naga:index_naga,nama_naga:nama_naga,hp_naga:hp_naga},
			function()
			{
				timer_wait_opponent = setInterval(
				function()
				{
					wait_opponent_dragon();
					console.log("waiting opponent");
				},1000);
			}
		);
	}
	
	//level up naga from food
	function addlevel(id_user,koor_x,koor_y, foodcount)
	{
		var foodcount = parseInt($("#number-food").html().substring($("#number-food").html().indexOf(":") + 1));
		if(foodcount - 1 >= 0)
		{
			$.post
			("levelup_naga.php", {id_user: id_user,koor_y: koor_y,koor_x:koor_x},
				function(data)
				{
					var lis_data=data.split(',');
					$('#level-dragon').html('Level:' +lis_data[0]);
					$('#number-food').html('Number Of Food:'+lis_data[1]);
					if(lis_data[0]=='20'){
						$.post("loadMap.php",function(data){build = 'none'; $("#container").html(data); initMap(); addHoverEvent();addClickEvent(); animateContainer();});
						$.post("give_food.php",{id_user: id_user,koor_x:lis_data[2],koor_y: lis_data[3]},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
					}
					else if(lis_data[0]=='30'){
						$.post("loadMap.php",function(data){build = 'none'; $("#container").html(data); initMap(); addHoverEvent();addClickEvent(); animateContainer();});
						$.post("give_food.php",{id_user: id_user,koor_x:lis_data[2],koor_y: lis_data[3]},function(data){$('#container').html(data);$("#container").css('display','none'); $("#container").fadeIn(500);});
					}
				}
			);
		}
		else
		{
			alert("Food Tidak Cukup!");
		}
		
	}

	//PLACING DRAGON FROM BREEDING TO MAP
	function insertNaga(type)
	{
		if($("#status-naga").html().toLowerCase() != 'sukses')
		{
			alert("Naga Belum Ready");
		}
		else
		{
			updateIndexSukses();			
			$.post
			("remove_naga.php", {nama_naga:naga,koor_y:index_i_sukses,koor_x:index_j_sukses,id_user:id_user},
				function(data)
				{
					index_i_sukses=-1;
					index_j_sukses=-1;
					naga=naga+"1";
				}
			);
			
			$.post("loadMap.php",function(data){naga = type;$("#container").html(data); initMap(); addHoverEvent();addClickEvent();});
		}
	}

	//ADDING TIME FOR BREEDING EGG
	function addtime()
	{
		$.post
		(
			"update_time.php", {id_user:id_user},
			function(data)
			{
				
			}
		);
		
		$.post
		(
			"update_gold_naga.php", {id_user:id_user},
			function(data)
			{
				
			}
		);
	}
	
	//UPDATING TIME REALTIME IN HATCHERY
	function update_time(id_user,koor_x,koor_y)
	{
		$.post
		("hatchery_update_time.php", {id_user:id_user,koor_x:koor_x,koor_y:koor_y},
			function(data)
			{
				var lis_data=data.split(',');
				$('#waktu-hatch').html(lis_data[0]);
				$('#status-naga').html(lis_data[1]);
			}
		);	
	}

	//FUNCTION CEK BANGUNAN
	function cek_bangunan(){
		$.post
		("cek_bangunan.php", {},
			function(hasil)
			{
				var lis_bangunan=hasil.split(';');
				for(var i=0;i<lis_bangunan.length-1;i++){
					var bangunan=lis_bangunan[i].split(',');
					var user=bangunan[0];
					var x=bangunan[1];
					var y=bangunan[2];
					var jenis=bangunan[3];
					if(id_user==user){
						map[y][x]=jenis;
					}
				}
			}
		);
	}
	
	//LOAD DATABASE ISI HABITAT
	function cek_habitat(){
		$.post
		("cek_habitat.php", {},
			function(hasil)
			{
				var lis_habitat=hasil.split(';');
				for(var i=0;i<lis_habitat.length-1;i++){
					var habitat=lis_habitat[i].split(',');
					var user=habitat[0];
					var x=habitat[1];
					var y=habitat[2];
					var jenis=habitat[3];
					if(id_user==user){
						map_naga[y][x]=jenis;
					}
				}
			}
		);
	}
	
	//ADDING USER'S FOOD IN DATABASE
	function createFood(food)
	{
		$.post("loadMap.php",function(data){jum_food=food;$("#container").html(data); initMap(); addHoverEvent();addClickEvent(); });
		$.post("updateFood.php", {food:food,id_user:id_user},function(data){
			if (data != "error") {
				$("#debug").html(data);
				$("#container").css('display','none'); $("#container").fadeIn(500);
			}else{
				alert("Insufficient gold");
			}
		}
		);
	}

	//ADDING MOUSE HOVER
	$(document).mousemove(function(event)
	{
		//CURSOR HOVER
		if(build!="none")
		{
			$('#orang').css('background-image','url("Resources/Images/'+build+'.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else if (naga!="none")
		{
			$('#orang').css('background-image','url("Resources/Images/'+naga+'.png")');
			$('#orang').css('left',event.pageX+10);
			$('#orang').css('top',event.pageY);
		}
		else
		{
			$('#orang').css('background-image','url("")');
		}
	});
	
	// Keyboard Event Cancel Build
	$(document).keypress(function(event)
	{
		if(event.key.toLowerCase() == "c")
		{
			// CANCEL BUILD
			build = "none";
			naga = "none";
		}
	});


	// Fade Animation For Container
	function animateContainer()
	{
		$("#container").css('display','none');
		$("#container").fadeIn(500);
	}
	
	//INTERVAL LOADING MAP AND ADDING TIME
	setInterval(function(){loadMap();addtime();},1000);
</script>

</html>
