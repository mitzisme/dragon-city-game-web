<?php
	require_once("connection.php");
	$editindex = $_POST['editindex'];
	$sql = "SELECT * FROM USERS WHERE ID = $editindex";
	$res = $conn->query($sql);
	$user = $res->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron bg-dark text-light">
			<form action="#">
				<div class="form-group">
					<label for="email">Email Address</label>
					<input type="email" name="email" id="email" class="form-control" readonly value="<?php echo $user['email'];?>">
				</div>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" class="form-control" value="<?php echo $user['name'];?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control" value="<?php echo $user['password'];?>">
				</div>
				<div class="form-group">
					<label for="gold">Gold</label>
					<input type="number" name="gold" id="gold" class="form-control" value="<?php echo $user['gold'];?>">
				</div>
				<div class="form-group">
					<label for="gem">Gem</label>
					<input type="number" name="gem" id="gem" class="form-control" value="<?php echo $user['gem'];?>">
				</div>
			</form>
			<button class="btn btn-danger" onclick="loadPageAdmin(1)">Cancel</button>
			<button class="btn btn-danger" onclick="updateUser(<?php echo $editindex;?>,0)">Delete User</button>
			<button class="btn btn-success" onclick="updateUser(<?php echo $editindex;?>,1)">Save Changes</button>
		</div>
	</div>
</body>
</html>

