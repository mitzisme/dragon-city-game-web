<?php
	require_once("connection.php");

	$suksesregister;

	if(isset($_POST['inputEmail']))
	{
		$email = $_POST['inputEmail'];
		
		// Search Email
		$sql = "SELECT * FROM users WHERE email LIKE '$email'";
		$result = $conn->query($sql);
		$count = mysqli_num_rows($result);
		if($count == 0)
		{
			//createUsername
			$password = $_POST['inputPassword'];
			$confPassword = $_POST['inputConfPassword'];
			$name = $_POST['inputName'];
			$umur = $_POST['umur'];

			if($password == $confPassword)
			{
				$sql = "INSERT INTO users (email,password,name,umur) VALUES ('$email','$password','$name', '$umur')";
				$conn->query($sql);
				$suksesregister = 1;
			}
			else
			{
				$suksesregister = 0;
			}
			
		}
		else
		{
			$suksesregister = -1;
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<!-- SCIRPTS + BOOSTRAP -->
	<script src="Jquery/jquery-3.3.1.min.js"></script>
	<script src="Bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
</head>
<body background="Resources/Images/bg1.jpg" style="background-size: 100%">
	
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <a class="navbar-brand" href="#">Dragon City</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">Register <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container col-md-3">
    	<?php
	    if(isset($suksesregister))
	    {
	    	if($suksesregister==-1)
	    	{
				echo "<div class='alert alert-danger text-center' id='alert' role='alert'>
				  Email Tersebut Tidak Tersedia!
				</div>";
	    	}
	    	else if($suksesregister == 1)
	    	{
	    		echo "<div class='alert alert-success text-center' id='alert' role='alert'>
					 Daftar Sukses!
					</div>";
	    	}
	    	else if($suksesregister == 0)
	    	{
	    		echo "<div class='alert alert-danger text-center' id='alert' role='alert'>
				  Password Dan Confirm Password Tidak Sama!
				</div>";
	    	}
	    }
	    ?>
    </div>
    

    <main role="main" class="container">
      <div style="float:none;margin:0 auto" class="jumbotron col-md-6">
         <form class="form-signin" method="POST" action="">
	      <div class="text-center mb-4">
	      <div class="form-label-group">
	       	<h6 class="text-left">Name: </h6>
	        <input type="text" name="inputName" id="inputName" class="form-control" placeholder="In Game Name" required autofocus>
	      </div>
	      <br>
	      <div class="form-label-group">
	       	<h6 class="text-left">Email address: </h6>
	        <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
	      </div>
			<br>
	      <div class="form-label-group">
	      	<h6 class="text-left">Password: </h6>
	        <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
	      </div>
			<br>
			<div class="form-label-group">
	      	<h6 class="text-left">Confirmation Password: </h6>
	        <input type="password" name="inputConfPassword" id="inputConfPassword" class="form-control" placeholder="Confirmation Password" required>
	      </div>
		  <div class="form-label-group">
	      	<h6 class="text-left">Umur: </h6>
	        <input type="text" name="umur" id="inputConfPassword" class="form-control" placeholder="Umur" required>
	      </div>
	      <br>
	      <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
	      <p class="mt-5 mb-3 text-muted text-center">&copy; 2018</p>
	    </form>
      </div>
    </main>
    <script>
    	var suksesregister = <?php echo $suksesregister;?>;

    	if(suksesregister == -1 || suksesregister == 0 || suksesregister==1)
    	{
    		$("#alert").fadeTo(2000, 500).slideUp(500, function(){
		    $("#alert").slideUp(500);
		});
    	}

    </script>
</body>
</html>