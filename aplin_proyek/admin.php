<?php
  require_once("connection.php");
	session_start();
	 
  if(!isset($_SESSION['userindex']))
  {
    header("location: login.php");
  }
  else
  {
    $userindex = $_SESSION['userindex'];
    $sql = "SELECT * FROM USERS WHERE ID = '$userindex'";
    $res = $conn->query($sql);
    $user = $res->fetch_assoc();
    
    if($user['isadmin'] != 1)
    {
      header("location: login.php");
    }

    if(isset($_POST['btnlogout']))
    {
      session_destroy();
      header("location: login.php");
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
	<!-- SCIRPTS + BOOSTRAP -->
	<script src="Jquery/jquery-3.3.1.min.js"></script>
	<script src="Bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
  <script type="text/javascript" src="DataTables/datatables.min.js"></script>
</head>
<body background="Resources/Images/binary.png" style="background-size: 100%">
	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <a class="navbar-brand" href="#">Dragon City</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active" id='lihome'>
            <a class="nav-link" href="#" onclick="loadPageAdmin(0)">Home </a>
          </li>
          <li class="nav-item" id='liusers'>
            <a class="nav-link" href="#" onclick="loadPageAdmin(1)">Users</a>
          </li> 
          <li class="nav-item" id='lihistory'>
            <a class="nav-link" href="#" onclick="loadPageAdmin(2)">History Penjualan</a>
          </li>
        </ul>
        <form class="form-inline" action="#" method="post">
        <ul class="navbar-nav mr-auto" style="padding-right: 10px; color:white;">
          <li>Welcome, <?php echo $user['name']." !";?></li>
        </ul>
        <button class="btn btn-outline-danger my-2 my-sm-0" name='btnlogout' type="submit">Logout</button>
      </form>
      </div>
    </nav>
    <!-- CONTENT -->
    <div class="container" name="pageContainer" id="pageContainer">
    </div>

    <!-- SCRIPTS -->
    <script>
    var userindex = <?php echo $userindex; ?>;

    //loadAdminPages
    function loadPageAdmin(page)
    {
      if(page == 0)
      {
        $("#liusers").removeClass("active");
        $("#lihome").addClass("active");
        $("#lihistory").removeClass("active");
        $.post("adm_home.php",{username: "<?php echo $user['name']?>"},function(data){$("#pageContainer").html(data); $("#pageContainer").css('display','none'); $("#pageContainer").fadeIn(500);});
      }
      else if(page == 1)
      {
        //GOTO USER MANAGEMENT PAGE (LISTUSER)
        $.post("user_mgmt.php",{userindex: userindex},function(data){$("#pageContainer").html(data); $('#tableUser').DataTable(); $("#pageContainer").css('display','none'); $("#pageContainer").fadeIn(500);});
        $("#lihome").removeClass("active");
        $("#lihistory").removeClass("active");
        $("#liusers").addClass("active");
      }
      else if(page == 2)
      {
        //GOTO HISTORY PENJUALAN PAGE
        $("#liusers").removeClass("active");
        $("#lihome").removeClass("active");
        $("#lihistory").addClass("active");
        $.post("history_penjulalan_adm.php", function(data){$("#pageContainer").html(data); $('#tableUser').DataTable(); $("#pageContainer").css('display','none'); $("#pageContainer").fadeIn(500);})
      }
    }

    function editUser(userid)
    {
      //GOTO USEREDITPAGE
      $.post("user_edit.php",{editindex: userid},function(data){$("#pageContainer").html(data);$("#pageContainer").css('display','none'); $("#pageContainer").fadeIn(500);});
    }

    function updateUser(userid, type)
    {
      if(type == 0)
      {
        //delete User
        if(userid == userindex)
        {
          alert("Tidak Bisa Delete Diri Sendiri");
        }
        else
        {
          if(confirm("Apakah Anda Yakin Akan Delete User Ini ?"))
          {
            //deleteUser
            $.post("user_update.php",{userindex: userid,type: type}, function(){alert("Sukses Delete User!");loadPageAdmin(1);});
          }
        }
      }
      else if(type == 1)
      {
        //updateUser
        $.post("user_update.php",{userindex: userid, 
                                name: $("#name").val(),
                                password: $("#password").val(),
                                gold: $("#gold").val(),
                                gem: $("#gem").val(),
                                type: type
                              }, 
        function(){alert("Sukses Update User!");loadPageAdmin(1);});
      }
    }

    loadPageAdmin(0);

  </script>
</body>
</html>