-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2018 at 08:54 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyek_aplin`
--
CREATE DATABASE IF NOT EXISTS `proyek_aplin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `proyek_aplin`;

-- --------------------------------------------------------

--
-- Table structure for table `bangunan`
--

DROP TABLE IF EXISTS `bangunan`;
CREATE TABLE `bangunan` (
  `id_user` int(11) NOT NULL,
  `koor_x` int(2) NOT NULL,
  `koor_y` int(2) NOT NULL,
  `jenis` int(1) NOT NULL,
  `kode_increment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bangunan`
--

INSERT INTO `bangunan` (`id_user`, `koor_x`, `koor_y`, `jenis`, `kode_increment`) VALUES
(3, 3, 2, 2, 38),
(3, 1, 0, 3, 39),
(2, 3, 2, 1, 40),
(2, 1, 1, 1, 41),
(2, 3, 0, 1, 42),
(2, 4, 1, 1, 43),
(2, 2, 2, 1, 44),
(2, 3, 1, 2, 45),
(2, 1, 1, 1, 46),
(2, 3, 0, 1, 47),
(2, 2, 1, 1, 48),
(2, 2, 0, 1, 49),
(2, 1, 2, 2, 50),
(2, 4, 0, 3, 51),
(2, 3, 4, 2, 52),
(2, 1, 4, 4, 53),
(2, 2, 3, 2, 54),
(2, 4, 2, 2, 55),
(2, 3, 3, 3, 56),
(2, 0, 1, 3, 57),
(3, 2, 3, 4, 58),
(3, 2, 1, 4, 59),
(3, 3, 1, 1, 60),
(3, 1, 1, 3, 61),
(3, 4, 4, 2, 62),
(4, 2, 1, 4, 63),
(4, 3, 1, 3, 64),
(4, 4, 1, 2, 65),
(8, 3, 2, 4, 66),
(2, 0, 2, 3, 67),
(2, 4, 3, 2, 68),
(2, 1, 3, 3, 69);

-- --------------------------------------------------------

--
-- Table structure for table `breeding`
--

DROP TABLE IF EXISTS `breeding`;
CREATE TABLE `breeding` (
  `id_user` int(11) NOT NULL,
  `koor_x` int(2) NOT NULL,
  `koor_y` int(2) NOT NULL,
  `nama_naga` varchar(30) NOT NULL,
  `status_breeding` varchar(20) NOT NULL,
  `time_breeding` int(2) NOT NULL,
  `index_breeding` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `breeding`
--

INSERT INTO `breeding` (`id_user`, `koor_x`, `koor_y`, `nama_naga`, `status_breeding`, `time_breeding`, `index_breeding`) VALUES
(2, 3, 4, 'Frilled', 'sukses', 50, 42),
(2, 4, 2, 'Praetorian', 'sukses', 50, 43),
(2, 1, 2, 'Frilled', 'sukses', 50, 44),
(2, 3, 4, 'Praetorian', 'sukses', 50, 45),
(3, 3, 2, 'Frilled', 'sukses', 50, 46),
(4, 4, 1, 'Praetorian', 'sukses', 50, 47),
(2, 3, 4, 'Frilled', 'sukses', 50, 48),
(2, 3, 4, 'Frilled', 'sukses', 50, 49),
(2, 3, 4, 'Praetorian', 'sukses', 50, 50),
(2, 3, 4, 'Praetorian', 'sukses', 50, 51),
(2, 4, 2, 'Praetorian', 'sukses', 50, 52);

-- --------------------------------------------------------

--
-- Table structure for table `lobby`
--

DROP TABLE IF EXISTS `lobby`;
CREATE TABLE `lobby` (
  `index_find` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `naga`
--

DROP TABLE IF EXISTS `naga`;
CREATE TABLE `naga` (
  `id_user` int(11) NOT NULL,
  `koor_x` int(11) NOT NULL,
  `koor_y` int(11) NOT NULL,
  `nama_naga` varchar(50) NOT NULL,
  `index_naga` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `naga`
--

INSERT INTO `naga` (`id_user`, `koor_x`, `koor_y`, `nama_naga`, `index_naga`) VALUES
(2, 3, 3, 'Frilled1', 6),
(2, 0, 1, 'Praetorian1', 7),
(3, 1, 0, 'Frilled1', 8),
(3, 1, 0, 'Frilled1', 9),
(3, 1, 1, 'Frilled', 10),
(4, 3, 1, 'Praetorian1', 11),
(2, 0, 2, 'Frilled', 12);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `user1` int(11) NOT NULL,
  `user2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `user1`, `user2`) VALUES
(22, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id_user` int(11) NOT NULL,
  `kode` text NOT NULL,
  `nilai` int(11) NOT NULL,
  `banyak` int(11) NOT NULL,
  `index_shop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`id_user`, `kode`, `nilai`, `banyak`, `index_shop`) VALUES
(1, 'gold', 10000, 1, 1),
(2, 'gold', 25000, 2, 2),
(3, 'gold', 75000, 0, 3),
(4, 'gold', 225000, 0, 4),
(5, 'gold', 675000, 0, 5),
(6, 'gem', 5, 0, 6),
(7, 'gem', 10, 1, 7),
(8, 'gem', 25, 0, 8),
(9, 'gem', 50, 0, 9),
(10, 'gem', 100, 0, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gold` int(11) NOT NULL,
  `gem` int(11) NOT NULL,
  `food` int(5) NOT NULL,
  `win` int(11) NOT NULL,
  `mmr` int(11) NOT NULL,
  `isadmin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `gold`, `gem`, `food`, `win`, `mmr`, `isadmin`) VALUES
(1, 'darkzn98@gmail.com', 'jose123', 'darkzn98', 99999, 0, 0, 0, 0, 1),
(2, 'asd@asd', 'password', 'asd', 450000, 4250, 220, 0, 0, 0),
(3, 'zxc@gmail.com', 'zxc', 'zxc', 300000, 200, 35, 0, 0, 0),
(4, 'dfg@dfg', 'dfg', 'dfg', 0, 0, 0, 0, 0, 0),
(5, 'asda@123', 'asdasd', 'asda', 0, 0, 0, 0, 0, 0),
(7, 'admin@stts.edu', 'admin', 'admin_ggwp', 0, 0, 0, 0, 0, 1),
(8, 'qqwer@qweqwe', 'asdf', 'qwer', 0, 500, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bangunan`
--
ALTER TABLE `bangunan`
  ADD PRIMARY KEY (`kode_increment`);

--
-- Indexes for table `breeding`
--
ALTER TABLE `breeding`
  ADD PRIMARY KEY (`index_breeding`);

--
-- Indexes for table `lobby`
--
ALTER TABLE `lobby`
  ADD PRIMARY KEY (`index_find`);

--
-- Indexes for table `naga`
--
ALTER TABLE `naga`
  ADD PRIMARY KEY (`index_naga`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`index_shop`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bangunan`
--
ALTER TABLE `bangunan`
  MODIFY `kode_increment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `breeding`
--
ALTER TABLE `breeding`
  MODIFY `index_breeding` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `lobby`
--
ALTER TABLE `lobby`
  MODIFY `index_find` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `naga`
--
ALTER TABLE `naga`
  MODIFY `index_naga` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `index_shop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
